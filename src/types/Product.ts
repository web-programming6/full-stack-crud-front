export default interface Product {
  id?: number;
  name: string;
  price: number;
  createdDate?: Date;
  updateDate?: Date;
  deleteDate?: Date;
}
